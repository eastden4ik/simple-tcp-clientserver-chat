package su.sviridoff.client;

import ru.sviridoff.framework.logger.Logger;
import su.sviridoff.net.TCPConnection;
import su.sviridoff.net.TCPListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Client extends JFrame implements ActionListener, TCPListener {

    private static final String IP_ADDRESS = "127.0.0.1";
    private static final int PORT = 8181;
    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;

    private static Logger logger = new Logger("SVIRIDOFF-CLIENT");

    public static void main(String[] args) {
        SwingUtilities.invokeLater(Client::new);
    }

    private final JTextArea log = new JTextArea();
    private final JTextField fieldLogin = new JTextField("Enter login");
    private final JTextField fieldInput = new JTextField();

    private TCPConnection connection;

    private Client() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);

        log.setEditable(false);
        log.setLineWrap(true);
        add(log, BorderLayout.CENTER);

        fieldInput.addActionListener(this);
        add(fieldInput, BorderLayout.SOUTH);
        add(fieldLogin, BorderLayout.NORTH);

        setVisible(true);
        try {
            connection = new TCPConnection(this, IP_ADDRESS, PORT);
        } catch (IOException e) {
            printMsg("Connection exception: " + e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String message = fieldInput.getText();
        if (message.equals("")) return;
        fieldInput.setText(null);
        connection.sendStringMessage(fieldLogin.getText() + ": " + message);
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMsg("Connection established!");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMsg(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMsg("Connection close!");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception ex) {
        printMsg("Connection exception: " + ex);
    }

    private synchronized void printMsg(String value) {
        logger.info(value);
        SwingUtilities.invokeLater(() -> {
            log.append(value + "\n");
            log.setCaretPosition(log.getDocument().getLength());
        });
    }
}
