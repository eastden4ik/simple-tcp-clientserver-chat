package su.sviridoff.net;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class TCPConnection {

    private final Socket socket;
    private Thread thread;
    private final BufferedReader in;
    private final BufferedWriter out;
    private final TCPListener eventListener;

    public TCPConnection(TCPListener eventListener, String ipAddress, int port) throws IOException {
        this(new Socket(ipAddress, port), eventListener);
    }

    public TCPConnection(Socket socket, TCPListener eventListener) throws IOException {
        this.eventListener = eventListener;
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        thread = new Thread(() -> {
            try {
                eventListener.onConnectionReady(TCPConnection.this);
                while (!thread.isInterrupted()) {
                    eventListener.onReceiveString(TCPConnection.this, in.readLine());
                }
            } catch (IOException exc) {
                eventListener.onException(TCPConnection.this, exc);
            } finally {
                eventListener.onDisconnect(TCPConnection.this);
            }
        });
        thread.start();
    }

    public synchronized void sendStringMessage(String value) {
        try {
            out.write(value + "\r\n");
            out.flush();
        } catch (IOException exc) {
            eventListener.onException(TCPConnection.this, exc);
            disconnect();
        }
    }

    public synchronized void disconnect() {
        thread.interrupt();
        try {
            socket.close();
        } catch (IOException exc) {
            eventListener.onException(TCPConnection.this, exc);
        }
    }

    @Override
    public String toString() {
        return "TCPConnection: [" + socket.getInetAddress() + ":" + socket.getPort() + "].";
    }

}
