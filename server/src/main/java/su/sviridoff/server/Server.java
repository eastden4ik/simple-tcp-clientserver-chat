package su.sviridoff.server;

import ru.sviridoff.framework.logger.Logger;
import su.sviridoff.net.TCPConnection;
import su.sviridoff.net.TCPListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class Server implements TCPListener {

    private final List<TCPConnection> connections = new ArrayList<>();

    private static Logger logger = new Logger("SVIRIDOFF-SERVER");

    public static void main(String[] args) {
        new Server();
    }

    private Server() {
        logger.setDebugLevel(true);
        logger.info("Server running...");
        try (ServerSocket socket = new ServerSocket(8181)) {
            while (true) {
                try {
                    new TCPConnection(socket.accept(), this);
                } catch (IOException exc) {
                    logger.error("TCPConnection exception: " + exc.getMessage());
                }
            }
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
    }

    @Override
    public synchronized void onConnectionReady(TCPConnection tcpConnection) {
        connections.add(tcpConnection);
        allNotification("Client connected: " + tcpConnection);
    }

    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
        allNotification(value);
    }

    @Override
    public synchronized void onDisconnect(TCPConnection tcpConnection) {
        connections.remove(tcpConnection);
        allNotification("Client disconnected: " + tcpConnection);
    }

    @Override
    public synchronized void onException(TCPConnection tcpConnection, Exception ex) {
        logger.error("TCPConnection exception: " + ex.getMessage());
    }

    private void allNotification(String value) {
        logger.debug(value);
        connections.forEach(tcpConnection -> tcpConnection.sendStringMessage(value));
    }
}
